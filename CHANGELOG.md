CHANGELOG
=========

3.0.0 (18/09/2019)
------------------

- Adaptation du code pour le passage à ZF3.
- Scission des classes liées aux entités ("Generic", "Group", "People", "Root", "Structure" et "System") en deux classes :
    - classe de base avec un setter pour (presque) chaque attribut Ldap
    - classe qui hérite de la classe de base avec des fonctions spécifiques à l'entité
- Gestions des attributs "supannActivite", "ucbnSecteurDisciplinaire" et "supannEmpCorps"
  
3.1.0 (01/10/2020)
------------------

- Ajout des getters pour les attributs de la classe "People" : "rid", "sambaSID", "uidNumber", "gidNumber", "loginShell" et "homeDirectory"
- Ajout des setters pour les différents attributs liés aux structures de la classe "People"
- Ajout des attributs cachés "createTimestamp" et "modifyTimestamp" comme DateTimeAttributes
- Ajout des setters pour les attributs de la classe "People" : "supannEtablissement", "supannEtuAnneeInscription", "supannEtuCursusAnnee", "supannEtuDiplome" et "supannEtuEtape"
- Ajout des setters pour les attributs de la classe "People" : "supannEtuInscription", "ucbnSiteLocalisation", "ucbnAnneePostBac", "ucbnCodeEtape", "ucbnEtuComplementInscription", 
"ucbnPrivateAddress", "ucbnPrivateAddresseBis", "supannEtuElementPedagogique", "supannEtuRegimeInscription", "supannEtuSecteurDisciplinaire" 
et "supannEtuTypeDiplome"
- Gestion de la modification de l'attribut "supannRefId" par étiquette
- Ajout de fonctions de vérification du statut d'une personne dans la classe "People"
- Fonction de récupération de champs date au format Php DateTime
- Mise en place d'une entité "Root" pour gérer la racine de l'annuaire Ldap et d'un service associé
- Ajout des setters pour les attributs de la classe "People" : "supannAutreMail" et "mailForwardingAddress"

3.1.1 (15/10/2020)
------------------

- Ajout des anciens attributs liés aux services : "ucbnService*"

3.1.2 (16/10/2020)
------------------

- Ajout des nouveaux attributs liés aux services : "unicaenService*"

3.1.5 (12/11/2020)
------------------

- Mise à jour des fonctions "setUnicaenTermsOfUse" et "setSupannRefId"

3.1.6 (16/11/2020)
------------------

- Gestion temporaire de l'attribut "campusPassword" pour pouvoir mettre à jour l'Active Directory

3.1.8 (02/12/2020)
------------------

- Ajout des attributs "unicaenSecuriteCompte" et "unicaenServiceEtat"

3.1.9 (04/01/2021)
------------------

- Ajout de l'attribut "unicaenRefId"

3.1.10 (02/02/2021)
-------------------

- Ajout de l'attribut "unicaenLeocarteCSN"
  
3.1.12 (22/02/2021)
-------------------

- Ajout de l'attribut "unicaenMailPrincipal"

3.1.13 (20/05/2021)
-------------------

- [Fix] L'attribut supannEmpCorps peut contenir des caractères spéciaux : -/

3.1.14 (02/06/2021)
-------------------

- [Fix] Prise en compte d'un diplôme inconnu ({INCONNU}) dans l'attribut supannEtuInscription

3.1.16 (29/11/2021)
-------------------

- Ajout de l'attribut "schacPersonnalUniqueCode" et de sa classe d'objet "schacLinkageIdentifiers"

3.1.17 (21/02/2022)
-------------------

- Ajout de l'attribut "unicaenRefProfil"
- Suppression des attributs "ucbnOrganisme" et "unicaenMonEtupass"

3.1.18 (16/03/2022)
-------------------

- Suppression de l'attribut "ucbnSquidHash"

3.1.21 (01/04/2022)
-------------------

- Suppression de la classe d'objet ucbnEntite qui n'est plus utilisé
- Ajout du fichier .gitlab-ci.yml