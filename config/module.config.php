<?php

namespace UnicaenLdap;

use UnicaenLdap\Entity\Generic;
use UnicaenLdap\Entity\Group;
use UnicaenLdap\Entity\People;
use UnicaenLdap\Entity\Structure;
use UnicaenLdap\Entity\System;
use UnicaenLdap\Options\ModuleOptionsFactory;
use UnicaenLdap\Service\Generic as LdapGenericService;
use UnicaenLdap\Service\GenericFactory as GenericServiceFactory;
use UnicaenLdap\Service\Group as LdapGroupService;
use UnicaenLdap\Service\GroupFactory as GroupServiceFactory;
use UnicaenLdap\Service\People as LdapPeopleService;
use UnicaenLdap\Service\PeopleFactory as PeopleServiceFactory;
use UnicaenLdap\Service\Root as LdapRootService;
use UnicaenLdap\Service\RootFactory as RootServiceFactory;
use UnicaenLdap\Service\Structure as LdapStructureService;
use UnicaenLdap\Service\StructureFactory as StructureServiceFactory;
use UnicaenLdap\Service\System as LdapSystemService;
use UnicaenLdap\Service\SystemFactory as SystemServiceFactory;
use Laminas\ServiceManager\Proxy\LazyServiceFactory;

return array(
    'unicaen-ldap' => [

    ],
    'service_manager' => [
        'factories' => [
            'Ldap'                      => LdapFactory::class,
            'LdapOptions'               => ModuleOptionsFactory::class,
            'LdapServiceGeneric'        => GenericServiceFactory::class,
            'LdapServiceGroup'          => GroupServiceFactory::class,
            'LdapServicePeople'         => PeopleServiceFactory::class,
            'LdapServiceRoot'           => RootServiceFactory::class,
            'LdapServiceStructure'      => StructureServiceFactory::class,
            'LdapServiceSystem'         => SystemServiceFactory::class,
        ],
        'aliases' => [
            'ldap'                      => 'Ldap',
            'ldapOptions'               => 'LdapOptions',
            'ldapServiceGeneric'        => 'LdapServiceGeneric',
            'ldapServiceGroup'          => 'LdapServiceGroup',
            'ldapServicePeople'         => 'LdapServicePeople',
            'ldapServiceRoot'           => 'LdapServiceRoot',
            'ldapServiceStructure'      => 'LdapServiceStructure',
            'ldapServiceSystem'         => 'LdapServiceSystem',
            LdapGenericService::class   => 'LdapServiceGeneric',
            LdapGroupService ::class    => 'LdapServiceGroup',
            LdapPeopleService::class    => 'LdapServicePeople',
            LdapRootService::class      => 'LdapServiceRoot',
            LdapStructureService::class => 'LdapServiceStructure',
            LdapSystemService::class    => 'LdapServiceSystem',
        ],
        'lazy_services' => [
            // Mapping services to their class names is required since the ServiceManager is not a declarative DIC.
            'class_map' => [
                'LdapServicePeople'     => LdapPeopleService::class,
                'LdapServiceGeneric'    => LdapGenericService::class,
                'LdapServiceGroup'      => LdapGroupService::class,
                'LdapServiceRoot'       => LdapRootService::class,
                'LdapServiceStructure'  => LdapStructureService::class,
                'LdapServiceSystem'     => LdapSystemService::class,
            ],
        ],
        'delegators' => [
            'LdapServicePeople' => [
                LazyServiceFactory::class,
            ],
            'LdapServiceGeneric' => [
                LazyServiceFactory::class,
            ],
            'LdapServiceGroup' => [
                LazyServiceFactory::class,
            ],
            'LdapServiceRoot' => [
                LazyServiceFactory::class,
            ],
            'LdapServiceStructure' => [
                LazyServiceFactory::class,
            ],
            'LdapServiceSystem' => [
                LazyServiceFactory::class,
            ],
        ],
    ],
);
