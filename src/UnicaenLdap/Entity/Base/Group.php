<?php

namespace UnicaenLdap\Entity\Base;

use UnicaenLdap\Entity\Entity;
use UnicaenLdap\Entity\People as PeopleEntity;
use UnicaenLdap\Entity\Structure as StructureEntity;
use UnicaenLdap\Entity\System as SystemEntity;
use UnicaenLdap\Exception;
use Laminas\Ldap\Dn;
use Laminas\Ldap\Exception\LdapException;

/**
 * Classe mère des entités de la branche "groups" de l'annuaire LDAP.
 *
 * @author David Surville <david.surville@unicaen.fr>
 */
class Group extends Entity
{
    /**
     * Membre par défaut d'un groupe lorsque le groupe est vide
     */
    const MEMBER_NOBODY = 'nobody';

    /**
     * @var string
     */
    protected $type = 'Group';

    /**
     * Liste des classes d'objet nécessaires à la création d'un groupe
     *
     * @var string[]
     */
    protected $objectClass = [
        'top',
        'groupOfNames',
        'supannGroupe',
    ];

    /**
     * Liste des attributs autorisés pour une entité "Group"
     *
     * @var array
     */
    protected $authorizedAttributes = [
        // Attributes classes
        'objectClass',
        // Attributes
        'description',
        'member',
        'owner',
        'supannGroupeDateFin',
        'supannGroupeLecteurDN',
        'supannGroupeAdminDN',
        'supannRefId',
    ];

    /**
     * Liste des attributs contenant des dates
     *
     * @var string[]
     */
    protected $dateTimeAttributes = [
        'supannGroupeDateFin',
    ];

    /**
     * Liste des attributs monovalués
     *
     * @var array
     */
    protected $monoValuedAttributes = [
        'supannGroupeDateFin',
    ];


    /**
     * Retourne le DN du membre par défaut
     *
     * @return string
     */
    public function getMemberNobody()
    {
        return sprintf('uid=%s,%s',
            self::MEMBER_NOBODY,
            $this->service->getLdapSystemService()->getBranches()[0]
        );
    }

    /**
     * Attribut Ldap "description"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     */
    public function setDescription($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('description', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "member"
     *
     * @param array|string|Dn|PeopleEntity|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setMember($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                return Dn::checkDn($val) ? $val : null;
            } elseif ($val instanceof Dn) {
                return $val->toString();
            } elseif ($val instanceof PeopleEntity) {
                return $val->getDn();
            } else {
                return null;
            }
        }, $value);

        $this->appendOrNot('member', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "owner"
     *
     * @param array|string|Dn|PeopleEntity|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setOwner($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                return Dn::checkDn($val) ? $val : null;
            } elseif ($val instanceof Dn) {
                return $val->toString();
            } elseif ($val instanceof PeopleEntity) {
                return $val->getDn();
            } else {
                return null;
            }
        }, $value);

        $this->appendOrNot('owner', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannGroupeDateFin"
     *
     * @param array|string|DateTime|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannGroupeDateFin($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                $val = new DateTime($val, new \DateTimeZone('+0000')); // définition du timezone à +0h
            }
            return (int)$val->format('U');
        }, $value);

        $this->appendOrNot('supannGroupeDateFin', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannGroupeLecteurDN"
     *
     * @param array|string|Dn|PeopleEntity|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannGroupeLecteurDN($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                return Dn::checkDn($val) ? $val : null;
            } elseif ($val instanceof Dn) {
                return $val->toString();
            } elseif ($val instanceof PeopleEntity) {
                return $val->getDn();
            } elseif ($val instanceof SystemEntity) {
                return $val->getDn();
            }
            else {
                return null;
            }
        }, $value);

        $this->appendOrNot('supannGroupeLecteurDN', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannGroupeAdminDN"
     *
     * @param array|string|Dn|PeopleEntity|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannGroupeAdminDN($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                return Dn::checkDn($val) ? $val : null;
            } elseif ($val instanceof Dn) {
                return $val->toString();
            } elseif ($val instanceof PeopleEntity) {
                return $val->getDn();
            } elseif ($val instanceof SystemEntity) {
                return $val->getDn();
            }
            else {
                return null;
            }
        }, $value);

        $this->appendOrNot('supannGroupeAdminDN', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannRefId"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannRefId($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$attribute_with_label_pattern, $v);
        });

        $this->appendOrNot('supannRefId', $value, $append);

        return $this;
    }
}