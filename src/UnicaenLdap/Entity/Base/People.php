<?php

namespace UnicaenLdap\Entity\Base;

use DateTime;
use UnicaenLdap\Entity\Entity;
use UnicaenLdap\Entity\People as PeopleEntity;
use UnicaenLdap\Entity\Structure as StructureEntity;
use UnicaenLdap\Exception;
use Laminas\Ldap\Attribute;
use Laminas\Ldap\Dn;
use Laminas\Ldap\Exception\LdapException;

/**
 * Classe mère des entités de la branche "people" de l'annuaire LDAP.
 *
 * @author David Surville <david.surville@unicaen.fr>
 */
class People extends Entity
{
    /**
     * Constantes pour les champs eduPersonAffiliation et eduPersonPrimaryAffiliation
     */
    const STATUS_AFFILIATE = 'affiliate';
    const STATUS_ALUM = 'alum';
    const STATUS_EMERITUS = 'emeritus';
    const STATUS_EMPLOYEE = 'employee';
    const STATUS_FACULTY = 'faculty';
    const STATUS_MEMBER = 'member';
    const STATUS_REGISTERED_READER = 'registered-reader';
    const STATUS_RESEARCHER = 'researcher';
    const STATUS_RETIRED = 'retired';
    const STATUS_STAFF = 'staff';
    const STATUS_STUDENT = 'student';
    const STATUS_TEACHER = 'teacher';

    /**
     * @var string
     */
    protected $type = 'People';

    /**
     * Liste des classes d'objet nécessaires à la création d'une entité "People"
     * Il est nécessaire d'ajouter la classe 'ucbnEtu' ou 'ucbnEmp' selon le
     * statut de la personne.
     *
     * @var array
     */
    protected $objectClass = [
        'top',
        'inetOrgPerson',
        'organizationalPerson',
        'person',
        'eduPerson',
        'supannPerson',
        'schacPersonalCharacteristics',
        'schacEntryMetadata',
        'schacLinkageIdentifiers',
        'ucbnEmp',
        'ucbnEtu',
        'posixAccount',
        'sambaAccount',
        'sambaSamAccount',
    ];

    /**
     * Liste des attributs autorisés pour une entité "People"
     *
     * @var array
     */
    protected $authorizedAttributes = [
        // Attributes classes
        'objectClass',
        // Attributes
        'campusPassword',
        'cn',
        'dateDeNaissance',
        'dateFinInscription',
        'displayName',
        'eduPersonAffiliation',
        'eduPersonNickname',
        'eduPersonOrgDN',
        'eduPersonOrgUnitDN',
        'eduPersonPrimaryAffiliation',
        'eduPersonPrimaryOrgUnitDN',
        'eduPersonPrincipalName',
        'facsimileTelephoneNumber',
        'gidNumber',
        'givenName',
        'homeDirectory',
        'labeledURI',
        'leoCode',
        'loginShell',
        'mail',
        'mailForwardingAddress',
        'mobile',
        'ntPassword',
        'pager',
        'postalAddress',
        'preferredLanguage',
        'rid',
        'sambaNTPassword',
        'sambaSID',
        'schacDateOfBirth',
        'schacExpiryDate',
        'schacPersonalUniqueCode',
        'sexe',
        'sn',
        'supannActivite',
        'supannAffectation',
        'supannAliasLogin',
        'supannAutreMail',
        'supannAutreTelephone',
        'supannCivilite',
        'supannCodeINE',
        'supannEmpCorps',
        'supannEmpId',
        'supannEntiteAffectation',
        'supannEntiteAffectationPrincipale',
        'supannEtablissement',
        'supannEtuAnneeInscription',
        'supannEtuCursusAnnee',
        'supannEtuDiplome',
        'supannEtuElementPedagogique',
        'supannEtuEtape',
        'supannEtuId',
        'supannEtuInscription',
        'supannEtuRegimeInscription',
        'supannEtuSecteurDisciplinaire',
        'supannEtuTypeDiplome',
        'supannListeRouge',
        'supannMailPerso',
        'supannParrainDN',
        'supannRoleGenerique',
        'supannRoleEntite',
        'supannRefId',
        'supannTypeEntiteAffectation',
        'telephoneNumber',
        'title',
        'ucbnAnneePostBac',
        'ucbnCodeEtape',
        'ucbnEtuComplementInscription',
        'ucbnFonctionStructurelle',
        'ucbnPrivateAddress',
        'ucbnPrivateAddressBis',
        'ucbnSecteurDisciplinaire',
        'ucbnServiceADE',
        'ucbnServiceAD',
        'ucbnServiceAnnuaire',
        'ucbnServiceEspacePerso',
        'ucbnServiceIMAP',
        'ucbnServicePapercut',
        'ucbnServiceRessourcesDoc',
        'ucbnServiceWIFI',
        'ucbnSiteLocalisation',
        'ucbnSousStructure',
        'ucbnStatus',
        'ucbnStructureRecherche',
        'uidNumber',
        'unicaenLeocarteCSN',
        'unicaenMailPrincipal',
        'unicaenRefId',
        'unicaenRefProfil',
        'unicaenSecuriteCompte',
        'unicaenServiceEtat',
        'unicaenTermsOfUse',
        'userCertificate',
        'userPassword',
    ];

    /**
     * Liste des attributs contenant des dates
     *
     * @var array
     */
    protected $dateTimeAttributes = [
        'schacExpiryDate',
    ];

    /**
     * Liste des attributs monovalués
     *
     * @var array
     */
    protected $monoValuedAttributes = [
        'cn',
        'dateDeNaissance',
        'dateFinInscription',
        'displayName',
        'eduPersonOrgDN',
        'eduPersonPrimaryAffiliation',
        'eduPersonPrimaryOrgUnitDN',
        'eduPersonPrincipalName',
        'gidNumber',
        'givenName',
        'homeDirectory',
        'leoCode',
        'loginShell',
        'ntPassword',
        'preferredLanguage',
        'rid',
        'sambaNTPassword',
        'sambaSID',
        'schacDateOfBirth',
        'schacExpiryDate',
        'sexe',
        'supannAliasLogin',
        'supannCivilite',
        'supannCodeINE',
        'supannEmpId',
        'supannEntiteAffectationPrincipale',
        'supannEtuId',
        'supannListeRouge',
        'telephoneNumber',
        'ucbnServiceADE',
        'ucbnServicePapercut',
        'uidNumber',
        'unicaenLeocarteCSN',
        'unicaenMailPrincipal',
        'unicaenRefId',
        'userPassword',
    ];

    /**
     * Liste des valeurs autorisées pour les attributs "eduPersonAffiliation" et "eduPersonPrimaryAffiliation"
     *
     * - affiliate : personne qui ne dépend pas de l'établissement (exemple : un partenaire extérieur). La valeur
     * "member" est exclue pour ces personnes.
     * - alum : ancien étudiant conservant des relations avec l'établissement.
     * - emeritus :  professeur ayant obtenu l'éméritat dans l'établissement.
     * - employee : tout personnel rémunéré par l'établissement, quelque soit son activité.
     * - faculty : personnel dont l'activité principale (dans l'établissement) est pédagogique, d'enseignement ou/et de
     * recherche. La valeur “member” est positionnée si ce personnel est géré par l'établissement.
     * - member : personne inscrite dans la (les) base(s) de gestion des étudiants ou celle(s) des personnels.
     * - registered-reader : lecteur de bibliothèque autorisé.
     * - researcher : personne assurant une activité de recherche. La valeur “member” est positionnée si ce personnel
     * est géré par l'établissement.
     * - retired :  personne à la retraite conservant des relations avec l'établissement.
     * - staff : personnel dont l'activité principale (dans l'établissement) est autre qu'enseignant ou chercheur
     * (typiquement BIATOS).
     * - student : personne suivant une formation quelconque dans l'établissement. Si la valeur “member” est positionnée,
     * la personne est enregistrée dans la base des étudiants.
     * - teacher : personnel assurant une activité d'enseignement.
     *
     * @var array
     */
    protected $eduPersonAffiliationAuthorizedValues = [
        self::STATUS_AFFILIATE,
        self::STATUS_ALUM,
        self::STATUS_EMERITUS,
        self::STATUS_EMPLOYEE,
        self::STATUS_FACULTY,
        self::STATUS_MEMBER,
        self::STATUS_REGISTERED_READER,
        self::STATUS_RESEARCHER,
        self::STATUS_RETIRED,
        self::STATUS_STAFF,
        self::STATUS_STUDENT,
        self::STATUS_TEACHER,
    ];

    /**
     * Liste des valeurs autorisées pour l'attribut "ucbnStatus"
     *
     * @var array
     */
    protected $ucbnStatusAuthorizedValues = [
        'ALUMNI',
        'APPRENANT',
        'AUDITEUR_LIBRE',
        'CONTRACTUEL',
        'ETUDIANT',
        'HEBERGE',
        'INVITE',
        'LECTEUR_SCD',
        'TITULAIRE',
    ];

    /**
     * Liste des patterns spécifiques utilisés pour différents attributs
     */
    static protected $structure_pattern = '/^(?<code>[\w\-]+);(?<libelle>.+)$/';
    static protected $localisation_pattern = '/^(?<code>[\w\-]+);(?<libelle>.+)$/';
    static protected $etape_pattern = '/^(?<code>.+);(?<libelle>.+)$/';
    static protected $secteur_disciplinaire_pattern = '/^(?<code>\d+);(?<libelle>.+)$/';
    static protected $inscription_pattern =
        '/^\[etab=(?<etab>\{[\w\-:]+\}.+)\]' .
        '\[anneeinsc=(?<anneeinsc>\d{4})\]' .
        '\[regimeinsc=(?<regimeinsc>\{[\w\-:]+\}\w*)\]' . // {SISE}.* ou {INCONNU}
        '\[sectdisc=(?<sectdisc>\{[\w\-:]+\}\w*)\]' . // {SISE}.* ou {INCONNU}
        '\[typedip=(?<typedip>\{[\w\-:]+\}\w*)\]' . // {SISE}.* ou {INCONNU}
        '\[cursusann=(?<cursusann>\{SUPANN\}\w+)\]' .
        '\[affect=(?<affect>[\w\-]+)\]' .
        '\[diplome=(?<diplome>\{[\w\-:]+\}\w*)\]' .
        '\[etape=(?<etape>\{[\w\-:]+\}.+)\]$/';
    static protected $inscription_complement_pattern = 
        '/^\[anneeinsc=(?<anneeinsc>\d{4})\]' .
        '\[etape=(?<etape>\{[\w\-:]+\}.+)\]' .
        '\[adistance=(?<adistance>\{[\w\-:]+\}\w{1})\]$/';
    static protected $role_pattern =
        '/^\[role=(?<role>\{SUPANN\}[\w\-]+)\]' .
        '\[type=(?<type>\{SUPANN\}[\w\-]+)\]' .
        '\[code=(?<code>[\w\-]+)\]' .
        '\[libelle=(?<libelle>.+)\]$/';
    static protected $role_src_pattern = '/^(?<code>\d{4});(?<libelle>.+)$/';
    static protected $service_etat_pattern = '/^(?<etiquette>\{[\w\-]+\})(?<value>(A|I|S){1})$/';
    static protected $personal_unique_code_pattern = '/^urn:schac:personalUniqueCode:(int:esi:)*(?<country_code>[\w]{2}):(?<code>.+)$/';


    /**
     * Attribut Ldap "sn"
     *
     * Nom d'un individu
     * Doit contenir le nom d'usage. Il est possible d'ajouter le nom de famille en seconde valeur.
     *
     * Multivalué
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSn($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map([$this, 'formatName'], $value);
        $this->appendOrNot('sn', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "givenName"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setGivenName($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map([$this, 'formatName'], $value);
        $this->appendOrNot('givenName', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "displayName"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setDisplayName($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map([$this, 'formatName'], $value);
        $this->appendOrNot('displayName', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "cn"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setCn($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map([$this, 'formatName'], $value, [true]);
        $this->appendOrNot('cn', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "dateDeNaissance"
     *
     * @param array|string|DateTime|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     * @deprecated Deprecated, use {@link self::setSchacDateOfBirth}
     *
     */
    public function setDateDeNaissance($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                $val = new DateTime($val);
            }
            return $val->format('Ymd');
        }, $value);

        $this->appendOrNot('dateDeNaissance', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "schacDateOfBirth"
     *
     * @param array|string|DateTime|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSchacDateOfBirth($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                $val = new DateTime($val);
            }
            return $val->format('Ymd');
        }, $value);

        $this->appendOrNot('schacDateOfBirth', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "schacPersonalUniqueCode"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setSchacPersonalUniqueCode($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$personal_unique_code_pattern, $v);
        });

        $this->appendOrNot('schacPersonalUniqueCode', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "dateFinInscription"
     *
     * @param array|string|DateTime|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     * @deprecated Deprecated, use {@link self::setSchacExpiryDate}
     *
     */
    public function setDateFinInscription($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                $val = new DateTime($val);
            }
            return $val->format('Ymd');
        }, $value);

        $this->appendOrNot('dateFinInscription', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "schacDateOfBirth"
     *
     * @param array|string|DateTime|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSchacExpiryDate($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                $val = new DateTime($val, new \DateTimeZone('+0000')); // définition du timezone à +0h
            }
            return (int)$val->format('U');
        }, $value);

        $this->appendOrNot('schacExpiryDate', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "sexe"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSexe($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return in_array($v, ['F', 'M']);
        });

        $this->appendOrNot('sexe', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "eduPersonAffiliation"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setEduPersonAffiliation($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return in_array($v, $this->eduPersonAffiliationAuthorizedValues);
        });

        $this->appendOrNot('eduPersonAffiliation', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "eduPersonPrimaryAffiliation"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setEduPersonPrimaryAffiliation($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return in_array($v, $this->eduPersonAffiliationAuthorizedValues);
        });

        $this->appendOrNot('eduPersonPrimaryAffiliation', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "postalAddress"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setPostalAddress($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$postal_address_pattern, $v);
        });

        $this->appendOrNot('postalAddress', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "telephoneNumber"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setTelephoneNumber($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map([$this, 'formatTel'], $value);
        $this->appendOrNot('telephoneNumber', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannAutreTelephone"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannAutreTelephone($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map([$this, 'formatTel'], $value);
        $this->appendOrNot('supannAutreTelephone', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "mail"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setMail($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter(filter_var_array($value, FILTER_VALIDATE_EMAIL));
        $this->appendOrNot('mail', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannAutreMail"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannAutreMail($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter(filter_var_array($value, FILTER_VALIDATE_EMAIL));
        $this->appendOrNot('supannAutreMail', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "mailForwardingAddress"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setMailForwardingAddress($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter(filter_var_array($value, FILTER_VALIDATE_EMAIL));
        $this->appendOrNot('mailForwardingAddress', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "mobile"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setMobile($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map([$this, 'formatTel'], $value);
        $this->appendOrNot('mobile', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "pager"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setPager($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map([$this, 'formatTel'], $value);
        $this->appendOrNot('pager', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "preferredLanguage"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setPreferredLanguage($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map('strtolower', $value);
        $this->appendOrNot('preferredLanguage', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "leoCode"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setLeoCode($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match('/^\d{13}$/', $v);
        });

        $this->appendOrNot('leoCode', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannALiasLogin"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannAliasLogin($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map('strtolower', $value);
        $value = array_filter($value, function ($v) {
            return preg_match('/^[0-9a-z\-]+$/', $v);
        });

        $this->appendOrNot('supannAliasLogin', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEmpId"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEmpId($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('supannEmpId', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEtuId"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEtuId($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('supannEtuId', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEtuInscription"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEtuInscription($value = null, $append = false)
    {
        $value = $this->preFormat($value);

        $supannLabel = $this->getLabel('SUPANN');
        $siseLabel = $this->getLabel('SISE');
        $inconnuLabel = $this->getLabel('INCONNU');
        $value = array_map(function ($v) use ($supannLabel, $siseLabel, $inconnuLabel) {
            if (preg_match(self::$inscription_pattern, $v, $matches)) {
                /**
                 * @var string $etab
                 * @var string $anneeinsc
                 * @var string $regimeinsc
                 * @var string $sectdisc
                 * @var string $typedip
                 * @var string $cursusann
                 * @var string $affect
                 * @var string $diplome
                 * @var string $etape
                 */
                foreach (['regimeinsc', 'sectdisc', 'typedip', 'diplome'] as $part) {
                    $$part = preg_match("/^($siseLabel|$inconnuLabel)\w*$/", $matches[$part])
                        ? $matches[$part]
                        : sprintf('%s%s', $siseLabel, $matches[$part]);
                }
                foreach (['cursusann'] as $part) {
                    $$part = preg_match("/^$supannLabel\w+$/", $matches[$part])
                        ? $matches[$part]
                        : sprintf('%s%s', $supannLabel, $matches[$part]);
                }

                $prefixe = $this->service->getLdapStructureService()->getCodeStructurePrefixe();
                $affect = (0 !== strpos($matches['affect'], $prefixe))
                    ? sprintf('%s%s', $structure_prefixe, $matches['affect'])
                    : $matches['affect'];

                $prefixe = $this->service->getLdapStructureService()->getCodeModuleEnseignementPrefixe();
                preg_match('/^(?<etiquette>\{[\w\-:]+\})(?<identifiant>.+)$/', $matches['etape'], $parts);
                $val = (0 !== strpos($parts['identifiant'], $prefixe))
                    ? $prefixe . $parts['identifiant']
                    : $parts['identifiant'];
                $etape = sprintf('%s%s', $parts['etiquette'], $val);

                return preg_replace(
                    self::$inscription_pattern,
                    "[etab=\\1][anneeinsc=\\2][regimeinsc=$regimeinsc][sectdisc=$sectdisc][typedip=$typedip][cursusann=$cursusann][affect=$affect][diplome=$diplome][etape=$etape]",
                    $v, 1);
            } else {
                return null;
            }

        }, $value);

        $this->appendOrNot('supannEtuInscription', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannCodeINE"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannCodeINE($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map('strtoupper', $value);
        $value = array_filter($value, function ($v) {
            return preg_match('/^[\w]{11}$/', $v);
        });

        $this->appendOrNot('supannCodeINE', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannCivilite"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannCivilite($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return in_array($v, ['Mme', 'M.']);
        });

        $this->appendOrNot('supannCivilite', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannListeRouge"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannListeRouge($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map([$this, 'formatBoolean'], $value);
        $this->appendOrNot('supannListeRouge', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannMailPerso"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannMailPerso($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter(filter_var_array($value, FILTER_VALIDATE_EMAIL));
        $this->appendOrNot('supannMailPerso', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannAffectation"
     *
     * @param array|string|StructureEntity|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannAffectation($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                return preg_match(self::$structure_pattern, $val) ? $val : null;
            } elseif ($val instanceof StructureEntity) {
                $val = sprintf('%s;%s', strtr($val->get('supannCodeEntite'), $this->service->getLdapStructureService()->getCodeStructurePrefixe(), ''), $val->get('description'));
                return preg_match(self::$structure_pattern, $val) ? $val : null;
            } else {
                return null;
            }
        }, $value);

        $this->appendOrNot('supannAffectation', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEntiteAffectation"
     *
     * @param array|string|StructureEntity|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEntiteAffectation($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                if (0 !== strpos($val, $this->service->getLdapStructureService()->getCodeStructurePrefixe())) {
                    $val = $this->service->getLdapStructureService()->getCodeStructurePrefixe() . $val;
                }
                return $val;
            } elseif ($val instanceof StructureEntity) {
                return $val->get('supannCodeEntite');
            } else {
                return null;
            }
        }, $value);

        $this->appendOrNot('supannEntiteAffectation', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEntiteAffectationPrincipale"
     *
     * @param array|string|StructureEntity|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEntiteAffectationPrincipale($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                if (0 !== strpos($val, $this->service->getLdapStructureService()->getCodeStructurePrefixe())) {
                    $val = $this->service->getLdapStructureService()->getCodeStructurePrefixe() . $val;
                }
                return $val;
            } elseif ($val instanceof StructureEntity) {
                return $val->get('supannCodeEntite');
            } else {
                return null;
            }
        }, $value);

        $this->appendOrNot('supannEntiteAffectationPrincipale', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannTypeEntiteAffectation"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannTypeEntiteAffectation($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $supannLabel = $this->getLabel('SUPANN');
        $value = array_map(function ($val) use ($supannLabel) {
            if (is_string($val)) {
                if (preg_match("/^" . $supannLabel . "[\w\-]+$/", $val)) {
                    return $val;
                } elseif (preg_match("/^[\w\-]+$/", $val)) {
                    return sprintf('%s%s', $supannLabel, $val);
                }
            }
            return null;
        }, $value);

        $this->appendOrNot('supannTypeEntiteAffectation', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "ucbnSiteLocalisation"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setUcbnSiteLocalisation($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$localisation_pattern, $v);
        });

        $this->appendOrNot('ucbnSiteLocalisation', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "ucbnSousStructure"
     *
     * @param array|string|StructureEntity|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setUcbnSousStructure($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                return preg_match(self::$structure_pattern, $val) ? $val : null;
            } elseif ($val instanceof StructureEntity) {
                $val = sprintf('%s;%s', strtr($val->get('supannCodeEntite'), $this->service->getLdapStructureService()->getCodeStructurePrefixe(), ''), $val->get('description'));
                return preg_match(self::$structure_pattern, $val) ? $val : null;
            } else {
                return null;
            }
        }, $value);

        $this->appendOrNot('ucbnSousStructure', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "ucbnStructureRecherche"
     *
     * @param array|string|StructureEntity|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setUcbnStructureRecherche($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                return preg_match(self::$structure_pattern, $val) ? $val : null;
            } elseif ($val instanceof StructureEntity) {
                $val = sprintf('%s;%s', strtr($val->get('supannCodeEntite'), $this->service->getLdapStructureService()->getCodeStructurePrefixe(), ''), $val->get('description'));
                return preg_match(self::$structure_pattern, $val) ? $val : null;
            } else {
                return null;
            }
        }, $value);

        $this->appendOrNot('ucbnStructureRecherche', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "eduPersonPrincipalName"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setEduPersonPrincipalName($value = null, $append = false)
    {
        $universiteDomaine = $this->service->getLdapRootService()->getEtablissementDomain();
        $value = $this->preFormat($value);
        $value = array_map(function ($v) use ($universiteDomaine) {
            list($identifiant, $domaine) = array_pad(explode('@', strtolower($v)), 2, null);
            if ($domaine != $universiteDomaine) {
                $v = sprintf('%s@%s', $v, $universiteDomaine);
            }
            return $v;
        }, $value);
        $value = array_filter($value, function ($v) use ($universiteDomaine) {
            return preg_match('/^[0-9a-z\-]+@' . $universiteDomaine . '$/', $v);
        });

        $this->appendOrNot('eduPersonPrincipalName', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "eduPersonOrgDN"
     *
     * @param array|string|Dn|StructureEntity|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setEduPersonOrgDN($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                return Dn::checkDn($val) ? $val : null;
            } elseif ($val instanceof Dn) {
                return $val->toString();
            } elseif ($val instanceof StructureEntity) {
                return $val->getDn();
            } else {
                return null;
            }
        }, $value);

        $this->appendOrNot('eduPersonOrgDN', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "eduPersonOrgUnitDN"
     *
     * @param array|string|Dn|StructureEntity|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setEduPersonOrgUnitDN($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                return Dn::checkDn($val) ? $val : null;
            } elseif ($val instanceof Dn) {
                return $val->toString();
            } elseif ($val instanceof StructureEntity) {
                return $val->getDn();
            } else {
                return null;
            }
        }, $value);

        $this->appendOrNot('eduPersonOrgUnitDN', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "eduPersonPrimaryOrgUnitDN"
     *
     * @param array|string|Dn|StructureEntity|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setEduPersonPrimaryOrgUnitDN($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                return Dn::checkDn($val) ? $val : null;
            } elseif ($val instanceof Dn) {
                return $val->toString();
            } elseif ($val instanceof StructureEntity) {
                return $val->getDn();
            } else {
                return null;
            }
        }, $value);

        $this->appendOrNot('eduPersonPrimaryOrgUnitDN', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannRoleGenerique"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannRoleGenerique($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $supannLabel = $this->getLabel('SUPANN');
        $value = array_map(function ($val) use ($supannLabel) {
            if (is_string($val)) {
                if (preg_match("/^" . $supannLabel . "[\w\-]+$/", $val)) {
                    return $val;
                } elseif (preg_match("/^[\w\-]+$/", $val)) {
                    return sprintf('%s%s', $supannLabel, $val);
                }
            }
            return null;
        }, $value);

        $this->appendOrNot('supannRoleGenerique', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannRoleEntite"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannRoleEntite($value = null, $append = false)
    {
        $value = $this->preFormat($value);

        $supannLabel = $this->getLabel('SUPANN');
        $value = array_map(function ($v) use ($supannLabel) {
            if (preg_match(self::$role_pattern, $v, $matches)) {
                /**
                 * @var string $role
                 * @var string $type
                 * @var string $code
                 */
                foreach (['role', 'type'] as $part) {
                    $$part = preg_match("/^" . $supannLabel . "[\w\-]+$/", $matches[$part])
                        ? $matches[$part]
                        : sprintf('%s%s', $supannLabel, $matches[$part]);
                }

                $prefixe = $this->service->getLdapStructureService()->getCodeStructurePrefixe();
                $code = (0 !== strpos($matches['code'], $prefixe))
                    ? sprintf('%s%s', $prefixe, $matches['code'])
                    : $matches['code'];

                return preg_replace(self::$role_pattern, "[role=$role][type=$type][code=$code][libelle=\\4]", $v, 1);
            } else {
                return null;
            }

        }, $value);

        $this->appendOrNot('supannRoleEntite', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "ucbnAnneePostBac"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setUcbnAnneePostBac($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match('/^\d{1}$/', $v);
        });

        $this->appendOrNot('ucbnAnneePostBac', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "ucbnCodeEtape"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setUcbnCodeEtape($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$etape_pattern, $v);
        });

        $this->appendOrNot('ucbnCodeEtape', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "ucbnEtuComplementInscription"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setUcbnEtuComplementInscription($value = null, $append = false)
    {
        $value = $this->preFormat($value);

        $value = array_map(function ($v) {
            if (preg_match(self::$inscription_complement_pattern, $v, $matches)) {
                /**
                 * @var string $anneeinsc
                 * @var string $etape
                 * @var string $adistance
                 */
                $prefixe = $this->service->getLdapStructureService()->getCodeModuleEnseignementPrefixe();
                preg_match('/^(?<etiquette>\{[\w\-:]+\})(?<identifiant>.+)$/', $matches['etape'], $parts);
                $val = (0 !== strpos($parts['identifiant'], $prefixe))
                    ? $prefixe . $parts['identifiant']
                    : $parts['identifiant'];
                $etape = sprintf('%s%s', $parts['etiquette'], $val);

                return preg_replace(
                    self::$inscription_pattern,
                    "[anneeinsc=\\1][etape=$etape][adistance=\\3]",
                    $v, 1);
            } else {
                return null;
            }

        }, $value);

        $this->appendOrNot('ucbnEtuComplementInscription', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "ucbnFonctionStructurelle"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setUcbnFonctionStructurelle($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$role_src_pattern, $v);

        });

        $this->appendOrNot('ucbnFonctionStructurelle', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "ucbnPrivateAddress"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setUcbnPrivateAddress($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$postal_address_pattern, $v);
        });

        $this->appendOrNot('ucbnPrivateAddress', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "ucbnPrivateAddressBis"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setUcbnPrivateAddressBis($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$postal_address_pattern, $v);
        });

        $this->appendOrNot('ucbnPrivateAddressBis', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannParrainDN"
     *
     * @param array|string|Dn|PeopleEntity|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannParrainDN($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                return Dn::checkDn($val) ? $val : null;
            } elseif ($val instanceof Dn) {
                return $val->toString();
            } elseif ($val instanceof PeopleEntity) {
                return $val->getDn();
            } else {
                return null;
            }
        }, $value);

        $this->appendOrNot('supannParrainDN', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannRefId"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannRefId($value = null, $append = false)
    {
        $value = $this->preFormat($value);

        if(empty($value)) {
            $this->appendOrNot('supannRefId', $value, false);
        }

        if ($append) {
            $this->appendOrNot('supannRefId', $value, true);
        }

        $newValues = [];
        foreach ($value as $v) {
            if (preg_match(self::$attribute_with_label_pattern, $v, $matches)) {
                $newValues[$matches['etiquette']][] = $matches['identifiant'];
            }
        };

        foreach ($newValues as $label => $values) {
            $currentValues = $this->preFormat($this->supannRefId);
            array_walk($currentValues, function (&$cv) use ($label, $values) {
                if (preg_match("/^".$label."(?<identifiant>.+)$/", $cv, $matches)) {
                    if(!in_array($matches['identifiant'], $values)) {
                        $this->remove('supannRefId', $cv);
                        $cv = null;
                    }
                }
            });
            foreach($values as $v) {
                $v = $label.$v;
                if (!in_array($v, $currentValues)) {
                    $currentValues[] = $v;
                }
            }
            $this->appendOrNot('supannRefId', array_filter($currentValues), false);
        }

        return $this;
    }

    /**
     * Attribut Ldap "ucbnStatus"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setUcbnStatus($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return in_array($v, $this->ucbnStatusAuthorizedValues);
        });

        $this->appendOrNot('ucbnStatus', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "unicaenLeocarteCSN"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setUnicaenLeocarteCSN($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function($v) {
            return preg_match('/^[0-9A-F]{14}$/i', $v);
        });

        $this->appendOrNot('unicaenLeocarteCSN', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "unicaenMailPrincipal"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setUnicaenMailPrincipal($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter(filter_var_array($value, FILTER_VALIDATE_EMAIL));
        $this->appendOrNot('unicaenMailPrincipal', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "unicaenRefId"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setUnicaenRefId($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('unicaenRefId', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "unicaenRefProfil"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setUnicaenRefProfil($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function($v) {
            return preg_match('/^[\w]+$/i', $v);
        });

        $this->appendOrNot('unicaenRefProfil', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "unicaenServiceEtat"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setUnicaenServiceEtat($value = null, $append = false)
    {
        $value = $this->preFormat($value);

        if(empty($value)) {
            $this->appendOrNot('unicaenServiceEtat', $value, false);
        }

        if ($append) {
            $this->appendOrNot('unicaenServiceEtat', $value, true);
        }

        $newValues = [];
        foreach ($value as $v) {
            if (preg_match(self::$service_etat_pattern, $v, $matches)) {
                $newValues[$matches['etiquette']][] = $matches['value'];
            }
        };

        foreach ($newValues as $label => $values) {
            $currentValues = $this->preFormat($this->unicaenServiceEtat);
            array_walk($currentValues, function (&$cv) use ($label, $values) {
                if (preg_match("/^".$label."(?<value>.+)$/", $cv, $matches)) {
                    if(!in_array($matches['value'], $values)) {
                        $this->remove('unicaenServiceEtat', $cv);
                        $cv = null;
                    }
                }
            });
            foreach($values as $v) {
                $v = $label.$v;
                if (!in_array($v, $currentValues)) {
                    $currentValues[] = $v;
                }
            }
            $this->appendOrNot('unicaenServiceEtat', array_filter($currentValues), false);
        }

        return $this;
    }

    /**
     * Attribut Ldap "unicaenTermsOfUse"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setUnicaenTermsOfUse($value = null, $append = false)
    {
        $value = $this->preFormat($value);

        if(empty($value)) {
            $this->appendOrNot('unicaenTermsOfUse', $value, false);
        }

        if ($append) {
            $this->appendOrNot('unicaenTermsOfUse', $value, true);
        }

        $newValues = [];
        foreach ($value as $v) {
            if (preg_match(self::$attribute_with_label_pattern, $v, $matches)) {
                $newValues[$matches['etiquette']][] = $matches['identifiant'];
            }
        };

        foreach ($newValues as $label => $values) {
            $currentValues = $this->preFormat($this->unicaenTermsOfUse);
            array_walk($currentValues, function (&$cv) use ($label, $values) {
                if (preg_match("/^".$label."(?<identifiant>.+)$/", $cv, $matches)) {
                    if(!in_array($matches['identifiant'], $values)) {
                        $this->remove('unicaenTermsOfUse', $cv);
                        $cv = null;
                    }
                }
            });
            foreach($values as $v) {
                $v = $label.$v;
                if (!in_array($v, $currentValues)) {
                    $currentValues[] = $v;
                }
            }
            $this->appendOrNot('unicaenTermsOfUse', array_filter($currentValues), false);
        }

        return $this;
    }

    /**
     * Attribut Ldap "supannActivite"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannActivite($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$attribute_with_label_pattern, $v);
        });

        $this->appendOrNot('supannActivite', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "ucbnSecteurDisciplinaire"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setUcbnSecteurDisciplinaire($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$secteur_disciplinaire_pattern, $v);

        });

        $this->appendOrNot('ucbnSecteurDisciplinaire', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEmpCorps"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEmpCorps($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $corpsLabel = $this->getLabel('NCORPS');
        $value = array_map(function ($val) use ($corpsLabel) {
            if (is_string($val)) {
                if (preg_match("/^$corpsLabel.*$/", $val)) {
                    return $val;
                } elseif (preg_match("/^.*$/", $val)) {
                    return sprintf('%s%s', $corpsLabel, $val);
                }
            }
            return null;
        }, $value);

        $this->appendOrNot('supannEmpCorps', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEtablissement"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEtablissement($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$attribute_with_label_pattern, $v);
        });

        $this->appendOrNot('supannEtablissement', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEtuAnneeInscription"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEtuAnneeInscription($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match('/^\d{4}$/', $v);
        });

        $this->appendOrNot('supannEtuAnneeInscription', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEtuCursusAnnee"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEtuCursusAnnee($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $supannLabel = $this->getLabel('SUPANN');
        $value = array_map(function ($val) use ($supannLabel) {
            if (is_string($val)) {
                if (preg_match("/^$supannLabel\w+$/", $val)) {
                    return $val;
                } elseif (preg_match("/^\w+$/", $val)) {
                    return sprintf('%s%s', $supannLabel, $val);
                }
            }
            return null;
        }, $value);

        $this->appendOrNot('supannEtuCursusAnnee', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEtuDiplome"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEtuDiplome($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$attribute_with_label_pattern, $v);
        });

        $this->appendOrNot('supannEtuDiplome', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEtuEtape"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEtuEtape($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                if (preg_match(self::$attribute_with_label_pattern, $val, $matches)) {
                    $prefixe = $this->service->getLdapStructureService()->getCodeModuleEnseignementPrefixe();
                    $val = (0 !== strpos($matches['identifiant'], $prefixe))
                        ? $prefixe . $matches['identifiant']
                        : $matches['identifiant'];
                    return sprintf('%s%s', $matches['etiquette'], $val);
                }
                return null;
            }
            return null;
        }, $value);

        $this->appendOrNot('supannEtuEtape', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEtuElementPedagogique"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEtuElementPedagogique($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                if (preg_match(self::$attribute_with_label_pattern, $val, $matches)) {
                    $prefixe = $this->service->getLdapStructureService()->getCodeModuleEnseignementPrefixe();
                    $val = (0 !== strpos($matches['identifiant'], $prefixe))
                        ? $prefixe . $matches['identifiant']
                        : $matches['identifiant'];
                    return sprintf('%s%s', $matches['etiquette'], $val);
                }
                return null;
            }
            return null;
        }, $value);

        $this->appendOrNot('supannEtuElementPedagogique', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEtuRegimeInscription"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEtuRegimeInscription($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $siseLabel = $this->getLabel('SISE');
        $value = array_map(function ($val) use ($siseLabel) {
            if (is_string($val)) {
                if (preg_match("/^$siseLabel\w+$/", $val)) {
                    return $val;
                } elseif (preg_match("/^\w+$/", $val)) {
                    return sprintf('%s%s', $siseLabel, $val);
                }
            }
            return null;
        }, $value);

        $this->appendOrNot('supannEtuRegimeInscription', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEtuSecteurDisciplinaire"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEtuSecteurDisciplinaire($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $siseLabel = $this->getLabel('SISE');
        $value = array_map(function ($val) use ($siseLabel) {
            if (is_string($val)) {
                if (preg_match("/^$siseLabel\w+$/", $val)) {
                    return $val;
                } elseif (preg_match("/^\w+$/", $val)) {
                    return sprintf('%s%s', $siseLabel, $val);
                }
            }
            return null;
        }, $value);

        $this->appendOrNot('supannEtuSecteurDisciplinaire', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEtuTypeDiplome"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEtuTypeDiplome($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $siseLabel = $this->getLabel('SISE');
        $value = array_map(function ($val) use ($siseLabel) {
            if (is_string($val)) {
                if (preg_match("/^$siseLabel\w+$/", $val)) {
                    return $val;
                } elseif (preg_match("/^\w+$/", $val)) {
                    return sprintf('%s%s', $siseLabel, $val);
                }
            }
            return null;
        }, $value);

        $this->appendOrNot('supannEtuTypeDiplome', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "rid"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setRid($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match('/^\d+$/', $v);
        });
        $this->appendOrNot('rid', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "sambaSID"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSambaSID($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('sambaSID', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "uidNumber"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setUidNumber($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match('/^\d+$/', $v);
        });
        $this->appendOrNot('uidNumber', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "gidNumber"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setGidNumber($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match('/^\d+$/', $v);
        });
        $this->appendOrNot('gidNumber', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "loginShell"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setLoginShell($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('loginShell', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "homeDirectory"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setHomeDirectory($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('homeDirectory', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "userPassword"
     *
     * @param string $value
     * @return self
     * @throws LdapException
     */
    public function setUserPassword(string $value)
    {
        $this->getNode()->setPasswordAttribute($value, Attribute::PASSWORD_HASH_SHA, 'userPassword');

        return $this;
    }

    /**
     * Attribut Ldap "ntPassword"
     *
     * @param string $value
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setNtPassword(string $value)
    {
        $uni = '';
        $value = (string)$value;
        for ($i = 0; $i < strlen($value); $i++) {
            $a = ord($value{$i}) << 8;
            $uni .= sprintf('%X', $a);
        }
        $nthash = hash("md4", pack('H*', $uni), true);
        $this->appendOrNot('ntPassword', strtoupper(bin2hex($nthash)), false);

        return $this;
    }

    /**
     * Attribut Ldap "sambaNTPassword"
     * @param string $value
     * @return $this
     * @throws Exception
     * @throws LdapException
     * @see setNtPassword
     *
     */
    public function setSambaNTPassword(string $value)
    {
        $uni = '';
        $value = (string)$value;
        for ($i = 0; $i < strlen($value); $i++) {
            $a = ord($value{$i}) << 8;
            $uni .= sprintf('%X', $a);
        }
        $nthash = hash("md4", pack('H*', $uni), true);
        $this->appendOrNot('sambaNTPassword', strtoupper(bin2hex($nthash)), false);

        return $this;
    }

    /**
     * Attribut Ldap "campusPassword"
     *
     * @param string $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setCampusPassword(string $value)
    {
        // On calcule la taille de la clé pour l'algo Blowfish
        $cle_taille = mcrypt_module_get_algo_key_size(MCRYPT_BLOWFISH);
        // On calcule la taille du vecteur d'initialisation pour l'algo Blowfish et pour le mode CBC
        $iv_taille = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC);
        // On fabrique le vecteur d'initialisation, la constante MCRYPT_RAND permet d'initialiser un vecteur aléatoire
        $iv = mcrypt_create_iv($iv_taille, MCRYPT_RAND);
        // On calcule la taille de block pour l'algo Blowfish et pour le mode CBC
        $block_taille = mcrypt_get_block_size(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC);
        //Cle
        $cleSHA = hash('sha512', "Dans les miroirs chinois, dans le bleu des photos, dans le regard d'un chat, dans les ailes d'un oiseau, dans la force d'un arbre, dans la couleur de l'eau, je te survivrai");
        // On retaille la clé pour qu'elle soit compatible
        $key = substr($cleSHA, 0, $cle_taille);
        # On vérifie la taille du mot de passe qui doit être un multiple de la taille de block
        $pad_len = $block_taille - (strlen($value) % $block_taille);
        $padding = str_repeat(chr($pad_len), $pad_len);
        // On chiffre le mot de passe avec un éventuel complément (padding) pour arriver à la taille compatible
        $value = mcrypt_encrypt(MCRYPT_BLOWFISH, $key, $value . $padding, MCRYPT_MODE_CBC, $iv);
        // On encode en base64 le mot de passe chiffré avec son IV
        $value = base64_encode($iv . $value);

        $this->appendOrNot('campusPassword', $value, false);

        return $this;
    }
}