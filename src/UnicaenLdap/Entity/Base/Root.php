<?php

namespace UnicaenLdap\Entity\Base;

use UnicaenLdap\Entity\Entity;
use UnicaenLdap\Entity\Structure as StructureEntity;
use UnicaenLdap\Exception;
use Laminas\Ldap\Exception\LdapException;

/**
 * Classe mère de l'entité racine de l'annuaire LDAP.
 *
 * @author David Surville <david.surville@unicaen.fr>
 */
class Root extends Entity
{
    protected $type = 'Root';


    /**
     * Liste des classes d'objet nécessaires à la création d'une structure
     *
     * @var string[]
     */
    protected $objectClass = [
        'top',
        'dcObject',
        'organization',
        'eduOrg',
        'supannOrg',
    ];

    /**
     * Liste des attributs autorisés pour l'entité "Root"
     *
     * @var array
     */
    protected $authorizedAttributes = [
        // Attributes classes
        'objectClass',
        // Attributes
        'description',
        'eduOrgHomePageURI',
        'eduOrgLegalName',
        'eduOrgSuperiorURI',
        'eduOrgWhitePagesURI',
        'facsimileTelephoneNumber',
        'l',
        'o',
        'postalAddress',
        'supannEtablissement',
        'telephoneNumber',
    ];

    /**
     * Liste des attributs contenant des dates
     *
     * @var array
     */
    protected $dateTimeAttributes = [];

    /**
     * Liste des attributs monovalués
     *
     * @var array
     */
    protected $monoValuedAttributes = [
        'eduOrgLegalName',
    ];

    /**
     * Attribut Ldap "description"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setDescription($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('description', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "eduOrgHomePageURI"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setEduOrgHomePageURI($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter(filter_var_array($value, FILTER_VALIDATE_URL));
        $this->appendOrNot('eduOrgHomePageURI', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "eduOrgLegalName"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setEduOrgLegalName($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('eduOrgLegalName', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "eduOrgSuperiorURI"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setEduOrgSuperiorURI($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter(filter_var_array($value, FILTER_VALIDATE_URL));
        $this->appendOrNot('eduOrgSuperiorURI', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "eduOrgWhitePagesURI"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setEduOrgWhitePagesURI($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter(filter_var_array($value, FILTER_VALIDATE_URL));
        $this->appendOrNot('eduOrgWhitePagesURI', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "facsimileTelephoneNumber"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setFacsimileTelephoneNumber($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map([$this, 'formatTel'], $value);
        $this->appendOrNot('facsimileTelephoneNumber', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "l"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setL($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('l', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "o"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setO($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('o', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "postalAddress"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setPostalAddress($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$postal_address_pattern, $v);
        });

        $this->appendOrNot('postalAddress', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannEtablissement"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return $this
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannEtablissement($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$attribute_with_label_pattern, $v);
        });

        $this->appendOrNot('supannEtablissement', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "telephoneNumber"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setTelephoneNumber($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map([$this, 'formatTel'], $value);
        $this->appendOrNot('telephoneNumber', $value, $append);

        return $this;
    }

}