<?php

namespace UnicaenLdap\Entity\Base;

use UnicaenLdap\Entity\Entity;
use UnicaenLdap\Entity\Structure as StructureEntity;
use UnicaenLdap\Exception;
use Laminas\Ldap\Exception\LdapException;

/**
 * Classe mère des entités de la branche "structures" de l'annuaire LDAP.
 *
 * @author David Surville <david.surville@unicaen.fr>
 */
class Structure extends Entity
{
    protected $type = 'Structure';

    /**
     * Liste des classes d'objet nécessaires à la création d'une structure
     *
     * @var string[]
     */
    protected $objectClass = [
        'top',
        'organizationalUnit',
        'supannEntite',
    ];

    /**
     * Liste des attributs autorisés pour une entité "People"
     *
     * @var array
     */
    protected $authorizedAttributes = [
        // Attributes classes
        'objectClass',
        // Attributes
        'description',
        'facsimileTelephoneNumber',
        'ou',
        'postalAddress',
        'supannCodeEntiteParent',
        'supannRefId',
        'supannTypeEntite',
        'telephoneNumber',
    ];

    /**
     * Liste des attributs contenant des dates
     *
     * @var array
     */
    protected $dateTimeAttributes = [];

    /**
     * Liste des attributs monovalués
     *
     * @var array
     */
    protected $monoValuedAttributes = [];


    /**
     * Attribut Ldap "description"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setDescription($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('description', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "ou"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setOu($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('ou', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannCodeEntiteParent"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannCodeEntiteParent($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map(function ($val) {
            if (is_string($val)) {
                if (0 !== strpos($val, $this->service->getCodeStructurePrefixe())) {
                    $val = $this->service->getCodeStructurePrefixe() . $val;
                }
                return $val;
            } elseif ($val instanceof StructureEntity) {
                return $val->get('supannCodeEntite');
            } else {
                return null;
            }
        }, $value);

        $this->appendOrNot('supannCodeEntiteParent', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannTypeEntite"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannTypeEntite($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $supannLabel = $this->getLabel('SUPANN');
        $value = array_map(function ($val) use ($supannLabel) {
            if (is_string($val)) {
                return preg_match("/^$supannLabel.+$/", $val) ? $val : sprintf('%s%s', $supannLabel, $val);
            } else {
                return null;
            }
        }, $value);

        $this->appendOrNot('supannTypeEntite', array_filter($value), $append);

        return $this;
    }

    /**
     * Attribut Ldap "telephoneNumber"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setTelephoneNumber($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map([$this, 'formatTel'], $value);
        $this->appendOrNot('telephoneNumber', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "facsimileTelephoneNumber"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setFacsimileTelephoneNumber($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_map([$this, 'formatTel'], $value);
        $this->appendOrNot('facsimileTelephoneNumber', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "postalAddress"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setPostalAddress($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$postal_address_pattern, $v);
        });

        $this->appendOrNot('postalAddress', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "supannRefId"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     * @throws Exception
     * @throws LdapException
     */
    public function setSupannRefId($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $value = array_filter($value, function ($v) {
            return preg_match(self::$attribute_with_label_pattern, $v);
        });

        $this->appendOrNot('supannRefId', $value, $append);

        return $this;
    }
}