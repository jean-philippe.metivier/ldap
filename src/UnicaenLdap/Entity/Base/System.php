<?php

namespace UnicaenLdap\Entity\Base;

use UnicaenLdap\Entity\Entity;
use Laminas\Ldap\Attribute;
use Laminas\Ldap\Exception\LdapException;

/**
 * Classe mère des entités de la branche "system" de l'annuaire LDAP.
 *
 * @author David Surville <david.surville@unicaen.fr>
 */
class System extends Entity
{
    protected $type = 'System';

    /**
     * Liste des classes d'objet nécessaires à la création d'un compte système
     *
     * @var string[]
     */
    protected $objectClass = [
        'top',
        'inetOrgPerson',
        'organizationalPerson',
        'person',
        'supannPerson',
        'ucbnEmp',
    ];

    /**
     * Liste des attributs autorisés pour une entité "Generic"
     *
     * @var array
     */
    protected $authorizedAttributes = [
        // Attributes classes
        'objectClass',
        // Attributes
        'cn',
        'sn',
        'userPassword',
    ];

    /**
     * Liste des attributs contenant des dates
     *
     * @var string[]
     */
    protected $dateTimeAttributes = [
    ];

    /**
     * Attribut Ldap "cn"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     */
    public function setCn($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('cn', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "sn"
     *
     * @param array|string|null $value
     * @param bool $append
     * @return self
     */
    public function setSn($value = null, $append = false)
    {
        $value = $this->preFormat($value);
        $this->appendOrNot('sn', $value, $append);

        return $this;
    }

    /**
     * Attribut Ldap "userPassword"
     *
     * @param string $value
     * @return self
     * @throws LdapException
     */
    public function setUserPassword(string $value)
    {
        $this->getNode()->setPasswordAttribute($value, Attribute::PASSWORD_HASH_SHA, 'userPassword');

        return $this;
    }
}