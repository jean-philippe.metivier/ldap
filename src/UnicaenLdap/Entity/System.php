<?php

namespace UnicaenLdap\Entity;

use UnicaenLdap\Entity\Base\System as BaseSystem;

/**
 * Classe de gestion des entités de la branche "system" de l'annuaire LDAP.
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class System extends Entity
{}