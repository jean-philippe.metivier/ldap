<?php

namespace UnicaenLdap;

/**
 * Classe mère des exceptions levées par les applis Unicaen Ldap.
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class Exception extends \Exception
{
    
}
