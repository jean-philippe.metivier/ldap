<?php

namespace UnicaenLdap\Filter;

use Laminas\Ldap\Filter as LaminasFilter;

/**
 * Filtre de base
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class Filter extends LaminasFilter
{
    
}