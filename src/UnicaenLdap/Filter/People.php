<?php

namespace UnicaenLdap\Filter;

use Laminas\Ldap\Filter\AbstractFilter;
use Laminas\Ldap\Filter\MaskFilter;

/**
 * Filtres pour les personnes.
 */
class People extends Filter
{
    /**
     * Filtre de recherche par numéro Harpège exact.
     *
     * @param string|integer $numero Numéro Harpère de l'individu, ex: '5195'
     * @return AbstractFilter
     */
    public static function noIndividu($numero)
    {
        return self::equals('supannEmpId', sprintf("%08s", intval($numero)));
    }

    /**
     * Filtre de recherche par login utilisateur exact.
     *
     * @param string $login Login utilisateur
     * @return AbstractFilter
     */
    public static function username($login)
    {
        return self::equals('supannAliasLogin', $login);
    }

    /**
     * Filtre de recherche par nom puis prénom exacts.
     *
     * @param string $name
     * @return AbstractFilter
     * @deprecated La recherche par nom *puis* prénom exacts est-elle vraiment utile ??
     */
    public static function name($name)
    {
        return self::equals('cn', $name);
    }

    /**
     * Filtre de recherche par début du nom (puis prénom éventuellement)
     *
     * @param string $text Ex: 'gauthier', 'gauthier b'
     * @return AbstractFilter
     */
    public static function nameBegins($text)
    {
        return self::begins('cn', $text);
    }

    /**
     * Filtre de recherche par bout de nom (puis prénom éventuellement)
     *
     * @param string $text Ex: 'authier', 'authier b'
     * @return AbstractFilter
     */
    public static function nameContains($text)
    {
        return self::contains('cn', $text);
    }

    /**
     * Filtre de recherche par nom d'utilisateur (puis prénom éventuellement) exact ou par login utilisateur exact.
     *
     * @param string $nameOrUsername
     * @return AbstractFilter
     */
    public static function nameOrUsername($nameOrUsername)
    {
        return self::orFilter(
            self::username($nameOrUsername),
            self::name($nameOrUsername)
        );
    }

    /**
     * Filtre de recherche par début du nom d'utilisateur (puis prénom éventuellement) ou par login utilisateur exact.
     *
     * @param string $text Login utilisateur ou nom d'utilisateur
     * @return AbstractFilter
     */
    public static function nameBeginsOrUsernameIs($text)
    {
        return self::orFilter(
            self::username($text),
            self::nameBegins($text)
        );
    }

    /**
     * Filtre de recherche par rôle.
     *
     * @param string $role      Ex: 'R40'
     * @param string $type      Ex: 'S200'
     * @param string $structure Ex: 'HS_13'
     * @return AbstractFilter
     */
    public static function role($role = null, $type = null, $structure = null)
    {
        $mask = 'supannRoleEntite=[role={SUPANN}%s][type={SUPANN}%s][code=%s][libelle=*]';

        return self::string(
            self::unescapeValue(
                new MaskFilter($mask, $role ?: '*', $type ?: '*', $structure ?: '*')
            )
        );
    }
}