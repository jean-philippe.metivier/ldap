<?php

namespace UnicaenLdap\Service;

/**
 * Classe regroupant les opérations de recherche des comptes génériques dans l'annuaire LDAP.
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class Generic extends AbstractService
{
    /**
     * Type de l'entité
     *
     * @var string
     */
    protected $type = 'Generic';

    /**
     * Organizational Units
     *
     * @var array
     */
    protected $ou = [
        'generic'
    ];
}