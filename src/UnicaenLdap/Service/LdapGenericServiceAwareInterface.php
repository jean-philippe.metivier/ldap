<?php

namespace UnicaenLdap\Service;

use UnicaenLdap\Service\Generic as LdapGenericService;

interface LdapGenericServiceAwareInterface
{
    /**
     * @param LdapGenericService $ldapGenericService
     * @return mixed
     */
    public function setLdapGenericService(LdapGenericService $ldapGenericService);

    /**
     * @return LdapGenericService
     */
    public function getLdapGenericService();
}
