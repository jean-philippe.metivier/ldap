<?php

namespace UnicaenLdap\Service;

use UnicaenLdap\Service\Group as LdapGroupService;

interface LdapGroupServiceAwareInterface
{
    /**
     * @param LdapGroupService $ldapGroupService
     * @return mixed
     */
    public function setLdapGroupService(LdapGroupService $ldapGroupService);

    /**
     * @return LdapGroupService
     */
    public function getLdapGroupService();
}
