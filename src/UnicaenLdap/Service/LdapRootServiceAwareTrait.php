<?php

namespace UnicaenLdap\Service;

use UnicaenLdap\Service\Root as LdapRootService;

trait LdapRootServiceAwareTrait
{
    /**
     * @var LdapRootService
     */
    protected $ldapRootService;

    /**
     * @param LdapRootService $ldapRootService
     */
    public function setLdapRootService(LdapRootService $ldapRootService)
    {
        $this->ldapRootService = $ldapRootService;
    }

    /**
     * @return LdapRootService
     */
    public function getLdapRootService()
    {
        return $this->ldapRootService;
    }
}