<?php

namespace UnicaenLdap\Service;

use UnicaenLdap\Service\System as LdapSystemService;

interface LdapSystemServiceAwareInterface
{
    /**
     * @param LdapSystemService $ldapSystemService
     * @return mixed
     */
    public function setLdapSystemService(LdapSystemService $ldapSystemService);

    /**
     * @return LdapSystemService
     */
    public function getLdapSystemService();
}
