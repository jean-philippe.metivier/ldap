<?php

namespace UnicaenLdap\Service;

use Interop\Container\ContainerInterface;
use UnicaenLdap\Ldap;
use UnicaenLdap\Service;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 *
 *
 * @author Unicaen
 */
class PeopleFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|People
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var Ldap $ldap
         * @var Service\Generic $ldapGenericService
         * @var Service\Group $ldapGroupService
         * @var Service\Root $ldapRootService
         * @var Service\Structure $ldapStructureService
         * @var Service\System $ldapSystemService
         */
        $ldap                   = $container->get('ldap');
        $ldapGenericService     = $container->get('ldapServiceGeneric');
        $ldapGroupService       = $container->get('ldapServiceGroup');
        $ldapRootService        = $container->get('ldapServiceRoot');
        $ldapStructureService   = $container->get('ldapServiceStructure');
        $ldapSystemService      = $container->get('ldapServiceSystem');

        $service = new People();
        $service->setLdap($ldap);
        $service->setLdapGenericService($ldapGenericService);
        $service->setLdapGroupService($ldapGroupService);
        $service->setLdapRootService($ldapRootService);
        $service->setLdapStructureService($ldapStructureService);
        $service->setLdapSystemService($ldapSystemService);

        return $service;
    }
}
