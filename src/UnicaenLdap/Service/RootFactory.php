<?php

namespace UnicaenLdap\Service;

use Interop\Container\ContainerInterface;
use UnicaenLdap\Ldap;
use UnicaenLdap\Service;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 *
 *
 * @author Unicaen
 */
class RootFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|System
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var Ldap $ldap
         * @var Service\Structure $ldapStructureService
         */
        $ldap                   = $container->get('ldap');
        $ldapStructureService   = $container->get('ldapServiceStructure');

        $service = new Root();
        $service->setLdap($ldap);
        $service->setLdapStructureService($ldapStructureService);

        return $service;
    }
}
