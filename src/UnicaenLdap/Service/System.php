<?php

namespace UnicaenLdap\Service;

/**
 * Classe regroupant les opérations de recherche de structures dans l'annuaire LDAP.
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class System extends AbstractService
{
    /**
     * Type de l'entité
     *
     * @var string
     */
    protected $type = 'System';

    /**
     * Organizational Units
     *
     * @var array
     */
    protected $ou = [
        'system'
    ];
}